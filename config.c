#include "config.h"

void game_config_free(GameConfig *config){
    free(config);
}

size_t game_config_get_generations(GameConfig *config){
    return config->generations;
}

GameConfig *game_config_new_from_cli(int argc, char *argv[]){

    GameConfig* config = malloc(sizeof(GameConfig));
    config->debug = 0;
    config->silent = 0;
    config->generations = 20;

    int opt;
    while((opt = getopt(argc, argv, "dsn:")) != -1){
        switch(opt){
            case 'd':
                config->debug = 1;
                break;
            case 's':
                config->silent = 1;
                break;
            case 'n':
                config->generations = atoi(optarg);
                break;

        }
    }

    /* Make sure d and s aren't both true */
    if(config->debug == 1 && config->silent == 1){
        printf("Can't have both -s and -d flags at the same time.\n");
    }

    config->input_file = fopen(argv[optind],"r");

/*
    printf("Filename:%s\n", argv[optind]);
    printf("Debug:%d\n", config->debug);
    printf("Silent:%d\n", config->silent);
    printf("n_iter:%zd\n", config->generations);
*/
    return config;
}
