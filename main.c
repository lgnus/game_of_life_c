#include "game.h"
#include "config.h"

int main(int argc, char* argv[]){
    // get the config and create a new game
    GameConfig* config = game_config_new_from_cli(argc, argv);
    Game* game = game_new();

    // parse game board
    game_parse_board(game, config);

    // if n == 0
    if(config->generations == 0){
      printf("\nstate 0:\n");
      game_print_board(game);
    } else {
      int stop = 0; // boolean to trigger a stop on the loop
      for(int i = 0; i <= config->generations; i++){

            if(i == 0 && !config->silent){
                printf("\nstate %d:\n", i);
                game_print_board(game);
            }

            if( config->debug && i > 0 && !config->silent){
                printf("\nstate %d:\n", i);
                game_print_board(game);
            }

            stop = game_tick(game);

            if((stop || i==config->generations-1)&& !config->silent){
                printf("\nstate %d:\n", i+1);
                game_print_board(game);
                break;
            }
      }
    }
    game_free(game);
    game_config_free(config);


}
