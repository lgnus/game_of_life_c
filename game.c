/* Game of life implementation for concurrency and parallelism
 * class
 */
#include "game.h"
#include <cilk/cilk.h>

#define BUFFER 1024 /* buffer size */

void game_free(Game *game){
    free(game);
}


Game *game_new(void){
    Game* game = malloc(sizeof(*game));
    return game;
}

int game_parse_board(Game *game, GameConfig *config){
    char buffer[BUFFER]; /* arbitraty size cause screw ya */

    /* Read Rows */
    fgets(buffer, BUFFER, config->input_file);
    strtok(buffer, ":");
    game->rows = atoi(strtok(NULL, ":"));

    /* Read Columns */
    fgets(buffer, BUFFER, config->input_file);
    strtok(buffer, ":");
    game->cols = atoi(strtok(NULL, ":"));

    /* Read Map/Board */
    game->board = malloc(sizeof(char)*game->cols*game->rows);
    while(fgets(buffer, BUFFER, config->input_file) != NULL){
        strncat(game->board, buffer, sizeof(char)*game->cols);
    }

    /* Close the file */
    fclose(config->input_file);
}

void game_print_board(Game *game){
    for(int i=0; i < game->rows; i++){
        printf("%.*s\n", game->cols,game->board+(game->cols*i));
    }
}

int game_cell_is_alive(Game *game, size_t row, size_t col){
    return game->board[row*game->cols + col] == '#';
}

int game_cell_is_dead(Game *game, size_t row, size_t col){
    return game->board[row*game->cols + col] == '.';
}

int game_tick(Game *game){
    int state_change = 0;
    int all_dead = 1;

    /* create new game board */
    char* new = malloc(strlen(game->board) + 1);
    strcpy(new, game->board);

    /* Iterate all cells */
    for(int i=0; i < game->rows; i++){
        for(int j=0; j < game->cols; j++){
            /* get neighbour counter */
            int neighbours = 0;
            cilk_for(int x=-1; x <= 1; x++){
                for(int y=-1; y <= 1; y++){
                    /* do nothing if x and y are 0 (original cell) */
                    if(x != 0 || y != 0){
                        int xi = x+i;
                        int yj = y+j;

                        /* handle edges */
                        xi = (xi < 0) ? (game->rows-1) : (xi >= game->rows) ? (0) : xi ;
                        yj = (yj < 0) ? (game->cols-1) : (yj >= game->cols) ? (0) : yj ;

                        if(game_cell_is_alive(game, xi, yj)){
                            neighbours++;
                        }
                    }
                }
            }

            /* Apply changes according to the number of neighbours */
            if(neighbours < 2 || neighbours > 3){
                if(game_cell_is_alive(game, i, j)){
                    state_change = 1;
                }

                /* Cell dies */
                new[i*game->cols + j] = '.';
            }

            if(neighbours == 3){
                if(game_cell_is_dead(game, i, j)){
                    state_change = 1;
                }

                /* Cell lives */
                all_dead = 0;
                new[i*game->cols + j] = '#';
            }
        }
    }

    /* Update game board
       free mallocs */
    free(game->board);
    game->board = new;

    /* if board did not change or all cells were dead: */
    if(all_dead || !state_change){
        return 1;
    }
    return 0;

}
